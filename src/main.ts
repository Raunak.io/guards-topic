// import { RolesGuard } from './auth.guard';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // app.useGlobalGuards(new RolesGuard()) // set globally for application
  await app.listen(3000);
}
bootstrap();
