import { RolesGuard } from './auth.guard';
import { Controller, Get, UseGuards, SetMetadata } from '@nestjs/common';
import { AppService } from './app.service';



// creating our own decorator 
const Roles = (...roles:string[])=>SetMetadata('roles',roles);


@Controller()

@UseGuards(RolesGuard) // passed class instead of an instance  cann also pass instance
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  // @SetMetadata('roles',['admin']) // custom metadata decorator
  @Roles('admin')
  getHello(): string {
    return this.appService.getHello();
  }
}




// check main.ts