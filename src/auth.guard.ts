import {Injectable , CanActivate,ExecutionContext, UnauthorizedException} from '@nestjs/common';
import {Observable}  from 'rxjs';
import { Reflector } from '@nestjs/core';



@Injectable()

export class AuthGuard implements CanActivate{

    canActivate(context:ExecutionContext,):boolean|Promise<boolean>|Observable<boolean>{
        const request = context.switchToHttp().getRequest();
        return request;


    } // just a basic auth guard

}


@Injectable()

export class RolesGuard implements CanActivate{

    // canActivate(context:ExecutionContext,):boolean|Promise<boolean>|Observable<boolean>{
    //     const request = context.switchToHttp().getRequest();
    //     return true;


    // } this one is simple guard

    constructor(private reflector:Reflector){}
    canActivate(context:ExecutionContext,):boolean|Promise<boolean>|Observable<boolean>{
       const roles = this.reflector.get<string[]>('roles',context.getHandler());
       if(!roles){
           return true
       }

       
        const request = context.switchToHttp().getRequest();
        const user = request.user;
        return this.matchRoles(roles,user.roles);


    } 
    private matchRoles(role,userRole){
        if(role === userRole){
            return true;
        }else{
            throw new UnauthorizedException('you are not authorized');
        }
    }

}